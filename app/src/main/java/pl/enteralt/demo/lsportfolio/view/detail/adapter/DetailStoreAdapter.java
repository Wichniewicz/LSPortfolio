package pl.enteralt.demo.lsportfolio.view.detail.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.enteralt.demo.lsportfolio.R;
import pl.enteralt.demo.lsportfolio.model.pojo.details.Link;
import pl.enteralt.demo.lsportfolio.view.detail.holder.DetailStoreHolder;

/**
 * Adapter class for detail store item
 */
public class DetailStoreAdapter extends RecyclerView.Adapter<DetailStoreHolder> {

    /**
     * List of detail store items
     */
    private List<Link> detailStoreList = new ArrayList<>();

    /**
     * Listener for view holder
     */
    private DetailStoreHolder.DetailStoreHolderListener listener;

    public DetailStoreAdapter(@NonNull DetailStoreHolder.DetailStoreHolderListener listener) {
        this.listener = listener;
    }

    @Override
    public DetailStoreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_store_holder, parent, false);
        return new DetailStoreHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(DetailStoreHolder holder, int position) {
        holder.setItem(detailStoreList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return detailStoreList.size();
    }

    /**
     * Add detail store items to list
     *
     * @param detailStoreItems List of detail store items
     */
    public void addDetailStoreItems(List<Link> detailStoreItems) {
        detailStoreList.clear();
        detailStoreList.addAll(detailStoreItems);
        notifyDataSetChanged();
    }
}
