package pl.enteralt.demo.lsportfolio.injector.component;

import dagger.Component;
import pl.enteralt.demo.lsportfolio.injector.scope.PerFragment;
import pl.enteralt.demo.lsportfolio.view.applications.fragment.ApplicationsFragment;
import pl.enteralt.demo.lsportfolio.view.contact.fragment.ContactFragment;

/**
 * Component for fragments
 */
@PerFragment
@Component(dependencies = {AppComponent.class})
public interface FragmentComponent {

    /**
     * Inject dependencies into {@link ContactFragment}
     *
     * @param contactFragment {@link ContactFragment}
     */
    void inject(ContactFragment contactFragment);

    /**
     * Inject dependencies into {@link ApplicationsFragment}
     *
     * @param applicationsFragment {@link ApplicationsFragment}
     */
    void inject(ApplicationsFragment applicationsFragment);
}
