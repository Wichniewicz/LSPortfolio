
package pl.enteralt.demo.lsportfolio.model.pojo.applications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Data {

    @SerializedName("slide")
    @Expose
    private List<Slide> slide = new ArrayList<Slide>();
    @SerializedName("portfolio")
    @Expose
    private List<Portfolio> portfolio = new ArrayList<Portfolio>();
    @SerializedName("person")
    @Expose
    private List<Person> person = new ArrayList<Person>();
    @SerializedName("work")
    @Expose
    private List<Work> work = new ArrayList<Work>();

    /**
     * @return The slide
     */
    public List<Slide> getSlide() {
        return slide;
    }

    /**
     * @param slide The slide
     */
    public void setSlide(List<Slide> slide) {
        this.slide = slide;
    }

    /**
     * @return The portfolio
     */
    public List<Portfolio> getPortfolio() {
        return portfolio;
    }

    /**
     * @param portfolio The portfolio
     */
    public void setPortfolio(List<Portfolio> portfolio) {
        this.portfolio = portfolio;
    }

    /**
     * @return The person
     */
    public List<Person> getPerson() {
        return person;
    }

    /**
     * @param person The person
     */
    public void setPerson(List<Person> person) {
        this.person = person;
    }

    /**
     * @return The work
     */
    public List<Work> getWork() {
        return work;
    }

    /**
     * @param work The work
     */
    public void setWork(List<Work> work) {
        this.work = work;
    }

}
