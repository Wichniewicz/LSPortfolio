package pl.enteralt.demo.lsportfolio.injector.module;

import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import pl.enteralt.demo.lsportfolio.injector.scope.PerActivity;

/**
 * Module for Activity
 */
@Module
public class ActivityModule {

    /**
     * Activity class
     */
    private AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    /**
     * Provide activity class object
     *
     * @return Activity class
     */
    @PerActivity
    @Provides
    public AppCompatActivity provideActivity() {
        return activity;
    }
}
