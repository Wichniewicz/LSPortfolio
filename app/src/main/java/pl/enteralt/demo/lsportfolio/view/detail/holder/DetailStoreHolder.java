package pl.enteralt.demo.lsportfolio.view.detail.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.enteralt.demo.lsportfolio.R;
import pl.enteralt.demo.lsportfolio.model.pojo.details.Link;
import pl.enteralt.demo.lsportfolio.view.common.BaseViewHolder;

/**
 * View holder class for detail store item
 */
public class DetailStoreHolder extends BaseViewHolder<Link, DetailStoreHolder.DetailStoreHolderListener> {

    @Bind(R.id.detailStoreImage)
    ImageView detailStoreImage;

    /**
     * Parent context
     */
    private Context context;

    public DetailStoreHolder(View itemView, DetailStoreHolder.DetailStoreHolderListener listener) {
        super(itemView, listener);
        ButterKnife.bind(this, itemView);
        context = detailStoreImage.getContext();
    }

    @Override
    public void setItem(Link item, int position) {
        super.setItem(item, position);
        Picasso.with(context)
                .load(item.getImage())
                .into(detailStoreImage);
    }

    @OnClick(R.id.detailStoreImage)
    public void onDetailStoreClicked() {
        listener.onStoreItemClicked(item);
    }

    /**
     * Interface to communicate with parent component
     */
    public interface DetailStoreHolderListener {

        /**
         * Action for click on list element
         *
         * @param link Clicked link
         */
        void onStoreItemClicked(Link link);

    }
}
