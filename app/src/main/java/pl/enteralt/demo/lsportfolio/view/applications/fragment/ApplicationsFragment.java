package pl.enteralt.demo.lsportfolio.view.applications.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.enteralt.demo.lsportfolio.R;
import pl.enteralt.demo.lsportfolio.model.pojo.applications.Portfolio;
import pl.enteralt.demo.lsportfolio.presenter.applications.ApplicationsFragmentPresenter;
import pl.enteralt.demo.lsportfolio.presenter.applications.ApplicationsFragmentView;
import pl.enteralt.demo.lsportfolio.view.applications.adapter.PortfolioAdapter;
import pl.enteralt.demo.lsportfolio.view.applications.holder.PortfolioHolder;
import pl.enteralt.demo.lsportfolio.view.common.BaseFragment;
import pl.enteralt.demo.lsportfolio.view.detail.activity.DetailActivity;

public class ApplicationsFragment extends BaseFragment implements ApplicationsFragmentView, PortfolioHolder.PortfolioHolderListener, SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = ApplicationsFragment.class.getSimpleName();

    @Bind(R.id.applicationsRecyclerView)
    RecyclerView applicationsRecyclerView;

    @Bind(R.id.applicationsRefresh)
    SwipeRefreshLayout applicationsRefresh;

    @Inject
    ApplicationsFragmentPresenter presenter;

    /**
     * Adapter to display portolio items on list
     */
    private PortfolioAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_applications, container, false);
        ButterKnife.bind(this, view);
        initDaggerComponents();
        initViews();
        return view;
    }

    @Override
    protected void initDaggerComponents() {
        FragmentComponentInitializer.init().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.bindView(this);
        adapter = new PortfolioAdapter(this);
        applicationsRecyclerView.setAdapter(adapter);
        applicationsRefresh.setOnRefreshListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        applicationsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        startRetrievingPortfolios();
    }

    /**
     * Starts retrieving portfolios
     */
    private void startRetrievingPortfolios() {
        Snackbar.make(applicationsRefresh, R.string.receiving_data, Snackbar.LENGTH_LONG)
                .show();
        presenter.getPortfoliosFromRest();
    }

    @Override
    public void onPortfoliosGet(List<Portfolio> portfolios) {
        adapter.addPortfolios(portfolios);
        applicationsRefresh.setRefreshing(false);
    }

    @Override
    public void onPortfoliosGetError() {
        Snackbar.make(applicationsRefresh, R.string.applications_rest_call_error, Snackbar.LENGTH_SHORT)
                .show();
        applicationsRefresh.setRefreshing(false);
    }

    @Override
    public void onPortfolioClicked(Portfolio portfolio) {
        startActivity(new Intent(getActivity(), DetailActivity.class)
                .putExtra("applicationId", portfolio.getId()));
    }

    @Override
    public void onRefresh() {
        startRetrievingPortfolios();
    }
}
