package pl.enteralt.demo.lsportfolio.view.applications.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.enteralt.demo.lsportfolio.R;
import pl.enteralt.demo.lsportfolio.model.pojo.applications.Portfolio;
import pl.enteralt.demo.lsportfolio.view.common.BaseViewHolder;

/**
 * View holder class for portfolio
 */
public class PortfolioHolder extends BaseViewHolder<Portfolio, PortfolioHolder.PortfolioHolderListener> {

    @Bind(R.id.portfolioHolderIcon)
    ImageView portfolioIcon;

    @Bind(R.id.portfolioHolderName)
    TextView portfolioName;

    @Bind(R.id.portfolioLayout)
    LinearLayout portfolioLayout;

    /**
     * Parent context
     */
    private Context context;

    public PortfolioHolder(View itemView, PortfolioHolderListener listener) {
        super(itemView, listener);
        ButterKnife.bind(this, itemView);
        context = portfolioLayout.getContext();
    }

    /**
     * Set item to display in view holder
     *
     * @param item     Item to display
     * @param position Position of Item in adapter
     */
    @Override
    public void setItem(Portfolio item, int position) {
        super.setItem(item, position);
        portfolioName.setText(item.getName());
        Picasso.with(context)
                .load(item.getIcon())
                .fit()
                .into(portfolioIcon);
        portfolioLayout.setBackgroundResource(position % 2 == 1 ?
                R.drawable.applications_background_primary :
                R.drawable.applications_background_secondary);

    }

    @OnClick(R.id.portfolioLayout)
    public void onPortfolioClicked() {
        listener.onPortfolioClicked(item);
    }

    /**
     * Interface to communicate with parent component
     */
    public interface PortfolioHolderListener {

        /**
         * Action for click on list element
         *
         * @param portfolio Clickec portfolio
         */
        void onPortfolioClicked(Portfolio portfolio);
    }
}
