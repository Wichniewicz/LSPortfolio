package pl.enteralt.demo.lsportfolio.view.detail.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.enteralt.demo.lsportfolio.R;
import pl.enteralt.demo.lsportfolio.view.common.BaseViewHolder;

/**
 * View holder class for detail gallery item
 */
public class DetailGalleryHolder extends BaseViewHolder<String, DetailGalleryHolder.DetailGalleryHolderListener> {

    @Bind(R.id.detailGalleryImage)
    ImageView detailGalleryImage;

    /**
     * Parent context
     */
    private Context context;

    public DetailGalleryHolder(View itemView, DetailGalleryHolderListener listener) {
        super(itemView, listener);
        ButterKnife.bind(this, itemView);
        context = detailGalleryImage.getContext();
    }

    @Override
    public void setItem(String item, int position) {
        super.setItem(item, position);
        Picasso.with(context)
                .load(item)
                .fit()
                .into(detailGalleryImage);
    }

    /**
     * Interface to communicate with parent component
     */
    public interface DetailGalleryHolderListener {
        // Empty
    }
}
