package pl.enteralt.demo.lsportfolio.view.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import pl.enteralt.demo.lsportfolio.app.App;
import pl.enteralt.demo.lsportfolio.injector.component.DaggerFragmentComponent;
import pl.enteralt.demo.lsportfolio.injector.component.FragmentComponent;

/**
 * Base fragment class
 */
public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    /**
     * Initializer for {@link pl.enteralt.demo.lsportfolio.injector.component.FragmentComponent}
     */
    protected static class FragmentComponentInitializer {

        private FragmentComponentInitializer() {
        }

        /**
         * Init injector component.
         *
         * @return {@link pl.enteralt.demo.lsportfolio.injector.component.FragmentComponent}
         */
        public static FragmentComponent init() {
            return DaggerFragmentComponent.builder()
                    .appComponent(App.getAppComponent())
                    .build();
        }
    }

    /**
     * Initialize dagger component
     */
    protected abstract void initDaggerComponents();

    /**
     * Initialize views
     */
    protected abstract void initViews();
}
