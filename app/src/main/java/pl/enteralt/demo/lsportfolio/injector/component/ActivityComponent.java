package pl.enteralt.demo.lsportfolio.injector.component;

import android.support.v7.app.AppCompatActivity;

import dagger.Component;
import pl.enteralt.demo.lsportfolio.injector.module.ActivityModule;
import pl.enteralt.demo.lsportfolio.injector.scope.PerActivity;
import pl.enteralt.demo.lsportfolio.view.detail.activity.DetailActivity;
import pl.enteralt.demo.lsportfolio.view.main.activity.MainActivity;
import pl.enteralt.demo.lsportfolio.view.splash.activity.SplashActivity;

/**
 * Component for activities
 */
@PerActivity
@Component(dependencies = {AppComponent.class}, modules = {ActivityModule.class})
public interface ActivityComponent {

    /**
     * Inject dependencies into {@link MainActivity}
     *
     * @param mainActivity {@link MainActivity}
     */
    void inject(MainActivity mainActivity);

    /**
     * Inject dependencies into {@link SplashActivity}
     *
     * @param splashActivity {@link SplashActivity}
     */
    void inject(SplashActivity splashActivity);

    /**
     * Inject dependencies into {@link DetailActivity}
     *
     * @param detailActivity {@link DetailActivity}
     */
    void inject(DetailActivity detailActivity);

    /**
     * @return Activity
     */
    AppCompatActivity activity();
}
