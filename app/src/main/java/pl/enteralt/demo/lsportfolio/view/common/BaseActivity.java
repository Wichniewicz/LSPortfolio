package pl.enteralt.demo.lsportfolio.view.common;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import pl.enteralt.demo.lsportfolio.app.App;
import pl.enteralt.demo.lsportfolio.injector.component.ActivityComponent;
import pl.enteralt.demo.lsportfolio.injector.component.DaggerActivityComponent;
import pl.enteralt.demo.lsportfolio.injector.module.ActivityModule;
import timber.log.Timber;

/**
 * Base activity class
 */
public abstract class BaseActivity extends AppCompatActivity{

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        Timber.d("onCreate");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Timber.d("onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Timber.d("onDestroy");
    }

    /**
     * Replace fragment without committing transaction.
     *
     * @param containerRes Resource id of container for fragment.
     * @param fragment     Fragment to replace with container.
     * @param tag          Tag of fragment for transaction.
     * @param <T>          Type of fragment, have to extends from {@link BaseFragment}
     * @return Transaction to commit.
     */
    protected <T extends BaseFragment> FragmentTransaction replaceFragment(@IdRes int containerRes, T fragment, String tag) {
        return getSupportFragmentManager().beginTransaction().replace(containerRes, fragment, tag);
    }

     /**
     * Initializer for {@link ActivityComponent}
     */
    protected static class ActivityComponentInitializer {

        private ActivityComponentInitializer() {
        }

        /**
         * Init injector component.
         *
         * @param activity Activity for adding to dependencies graph.
         *                 Activity needs to extend {@link BaseActivity}
         * @return {@link ActivityComponent}.
         */
        public static <A extends BaseActivity> ActivityComponent init(A activity) {
            return DaggerActivityComponent.builder()
                    .appComponent(App.getAppComponent())
                    .activityModule(new ActivityModule(activity))
                    .build();
        }
    }
}
