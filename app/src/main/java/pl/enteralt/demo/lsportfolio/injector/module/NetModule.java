package pl.enteralt.demo.lsportfolio.injector.module;

import dagger.Module;
import dagger.Provides;
import pl.enteralt.demo.lsportfolio.injector.scope.PerApp;
import pl.enteralt.demo.lsportfolio.rest.RestApi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Module for network components (i.e. Retrofit)
 */
@Module
public class NetModule {

    /**
     * Base URL for retrofit calls
     */
    private String baseUrl;

    public NetModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * Provides retrofit class object
     *
     * @return retrofit object
     */
    @PerApp
    @Provides
    public Retrofit provideRetrofit() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    /**
     * Provides REST API class object
     *
     * @param retrofit retrofit object from DI
     * @return REST API object
     */
    @PerApp
    @Provides
    public RestApi provideRestApi(Retrofit retrofit) {

        RestApi service = retrofit.create(RestApi.class);
        return service;
    }
}
