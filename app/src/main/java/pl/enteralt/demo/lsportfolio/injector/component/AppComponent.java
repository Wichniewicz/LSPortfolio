package pl.enteralt.demo.lsportfolio.injector.component;

import dagger.Component;
import pl.enteralt.demo.lsportfolio.app.App;
import pl.enteralt.demo.lsportfolio.injector.module.AppModule;
import pl.enteralt.demo.lsportfolio.injector.module.NetModule;
import pl.enteralt.demo.lsportfolio.injector.scope.PerApp;
import pl.enteralt.demo.lsportfolio.rest.RestApi;

/**
 * Component for Application
 */
@PerApp
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {

    /**
     * @return Application dependency
     */
    App app();

    /**
     * Inject dependencies into {@link App}
     *
     * @param app {@link App}
     */
    void inject(App app);

    /**
     * @return REST API for accessing to the website
     */
    RestApi restApi();
}
