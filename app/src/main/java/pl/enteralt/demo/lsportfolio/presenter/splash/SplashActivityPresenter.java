package pl.enteralt.demo.lsportfolio.presenter.splash;

import android.os.Handler;

import javax.inject.Inject;

import pl.enteralt.demo.lsportfolio.injector.scope.PerActivity;
import pl.enteralt.demo.lsportfolio.presenter.Presenter;

/**
 * Presenter for {@link pl.enteralt.demo.lsportfolio.view.splash.activity.SplashActivity}
 */
@PerActivity
public class SplashActivityPresenter implements Presenter<SplashActivityView> {

    /**
     * Time of showing splash
     */
    private static int SPLASH_TIME = 2000;

    /**
     * View controlled by presenter
     */
    private SplashActivityView view;

    /**
     * Runnable for showing splash
     */
    private Runnable runnable;

    /**
     * Handler for showing splash
     */
    private Handler handler;

    @Inject
    public SplashActivityPresenter() {
        // Empty
    }

    /**
     * Bind view to control by presenter.
     *
     * @param view View to control by presenter.
     */
    @Override
    public void bindView(SplashActivityView view) {
        this.view = view;
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onResume() method.
     */
    @Override
    public void resume() {
        // Empty
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onPause() method.
     */
    @Override
    public void pause() {
        // Empty
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onDestroy() method.
     */
    @Override
    public void destroy() {
        handler.removeCallbacks(runnable);
    }

    /**
     * Starts delay for splash screen
     */
    public void startDelay() {

        runnable = new Runnable() {
            @Override
            public void run() {
                view.onSplashEnd();
            }
        };

        handler = new Handler();
        handler.postDelayed(runnable, SPLASH_TIME);
    }
}
