package pl.enteralt.demo.lsportfolio.view.applications.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.enteralt.demo.lsportfolio.R;
import pl.enteralt.demo.lsportfolio.model.pojo.applications.Portfolio;
import pl.enteralt.demo.lsportfolio.view.applications.holder.PortfolioHolder;

/**
 * Adapter class for portfolio
 */
public class PortfolioAdapter extends RecyclerView.Adapter<PortfolioHolder> {

    /**
     * List of portfolio items
     */
    private List<Portfolio> portfolioList = new ArrayList<>();

    /**
     * Listener for view holder
     */
    private PortfolioHolder.PortfolioHolderListener listener;

    public PortfolioAdapter(@NonNull PortfolioHolder.PortfolioHolderListener listener) {
        this.listener = listener;
    }

    @Override
    public PortfolioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.portfolio_holder, parent, false);
        return new PortfolioHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(PortfolioHolder holder, int position) {
        holder.setItem(portfolioList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return portfolioList.size();
    }

    /**
     * Add portfolios to list
     *
     * @param portfolios List of portolios
     */
    public void addPortfolios(List<Portfolio> portfolios) {
        portfolioList.clear();
        portfolioList.addAll(portfolios);
        notifyDataSetChanged();
    }
}
