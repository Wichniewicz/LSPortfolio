package pl.enteralt.demo.lsportfolio.injector.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Scope for whole fragment lifecycle
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerFragment {
}
