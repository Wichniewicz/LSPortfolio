package pl.enteralt.demo.lsportfolio.view.detail.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.biubiubiu.justifytext.library.JustifyTextView;
import pl.enteralt.demo.lsportfolio.R;
import pl.enteralt.demo.lsportfolio.model.pojo.details.Data;
import pl.enteralt.demo.lsportfolio.model.pojo.details.Link;
import pl.enteralt.demo.lsportfolio.presenter.detail.DetailActivityPresenter;
import pl.enteralt.demo.lsportfolio.presenter.detail.DetailActivityView;
import pl.enteralt.demo.lsportfolio.view.common.BaseActivity;
import pl.enteralt.demo.lsportfolio.view.detail.adapter.DetailGalleryAdapter;
import pl.enteralt.demo.lsportfolio.view.detail.adapter.DetailStoreAdapter;
import pl.enteralt.demo.lsportfolio.view.detail.holder.DetailGalleryHolder;
import pl.enteralt.demo.lsportfolio.view.detail.holder.DetailStoreHolder;
import timber.log.Timber;

public class DetailActivity extends BaseActivity implements DetailActivityView, DetailGalleryHolder.DetailGalleryHolderListener, DetailStoreHolder.DetailStoreHolderListener {

    @Bind(R.id.detailActivityToolbar)
    Toolbar toolbar;

    @Bind(R.id.commonToolbarTitle)
    TextView toolbarTitle;

    @Bind(R.id.detailLinearLayout)
    LinearLayout detailLayout;

    @Bind(R.id.detailName)
    TextView detailName;

    @Bind(R.id.detailDescription)
    JustifyTextView detailDescription;

    @Bind(R.id.detailIcon)
    ImageView detailIcon;

    @Bind(R.id.detailGallery)
    RecyclerView detailGalleryRecyclerView;

    @Bind(R.id.detailStoreLinks)
    RecyclerView detailStoreLinksRecyclerView;

    @Bind(R.id.detailScrollview)
    ScrollView detailScrollview;

    @Inject
    DetailActivityPresenter presenter;

    /**
     * Adapter to display gallery items on list
     */
    private DetailGalleryAdapter detailGalleryAdapter;

    /**
     * Adapter to display store items on list
     */
    private DetailStoreAdapter detailStoreAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setupToolbar();
        initViews();
        startRetrievingApplicationDetail();
    }

    /**
     * Starts retrieving application detail
     */
    private void startRetrievingApplicationDetail() {
        Snackbar.make(detailLayout, R.string.receiving_data, Snackbar.LENGTH_LONG)
                .show();
        presenter.getApplicationDetailFromRest(getIntent().getIntExtra("applicationId", 0));
    }

    /**
     * Initialize views
     */
    private void initViews() {
        ActivityComponentInitializer.init(this).inject(this);
        presenter.bindView(this);
        detailGalleryAdapter = new DetailGalleryAdapter(this);
        detailStoreAdapter = new DetailStoreAdapter(this);
        detailGalleryRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        detailStoreLinksRecyclerView.setLayoutManager(
                getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ?
                        new LinearLayoutManager(this) :
                        new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        );
        detailGalleryRecyclerView.setAdapter(detailGalleryAdapter);
        detailStoreLinksRecyclerView.setAdapter(detailStoreAdapter);
    }

    /**
     * Sets toolbar for activity
     */
    private void setupToolbar() {
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.back_icon);
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void onApplicationDetailGet(Data applicationDetail) {
        toolbarTitle.setText(applicationDetail.getName());
        detailName.setText(applicationDetail.getName());
        detailGalleryAdapter.addDetailGalleryItems(applicationDetail.getGallery());
        detailStoreAdapter.addDetailStoreItems(applicationDetail.getLink());
        // TODO Implement own solution for justifying text. This has problems.
        detailDescription.setText(applicationDetail.getDescription());
        Picasso.with(this)
                .load(applicationDetail.getIcon())
                .fit()
                .into(detailIcon);
        detailScrollview.fullScroll(View.FOCUS_UP);
    }

    @Override
    public void onApplicationDetailGetError() {
        Snackbar.make(detailLayout, R.string.application_detail_rest_call_error, Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onStoreItemClicked(Link link) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link.getUrl())));
        } catch (Exception e) {
            e.printStackTrace();
            Timber.e(e.getMessage());
        }
    }
}
