
package pl.enteralt.demo.lsportfolio.model.pojo.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Type {

    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("dx")
    @Expose
    private Integer dx;
    @SerializedName("dy")
    @Expose
    private Integer dy;

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param width The width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * @return The height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height The height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @return The dx
     */
    public Integer getDx() {
        return dx;
    }

    /**
     * @param dx The dx
     */
    public void setDx(Integer dx) {
        this.dx = dx;
    }

    /**
     * @return The dy
     */
    public Integer getDy() {
        return dy;
    }

    /**
     * @param dy The dy
     */
    public void setDy(Integer dy) {
        this.dy = dy;
    }

}
