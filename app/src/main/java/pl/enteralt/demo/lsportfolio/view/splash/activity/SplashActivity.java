package pl.enteralt.demo.lsportfolio.view.splash.activity;

import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;

import pl.enteralt.demo.lsportfolio.presenter.splash.SplashActivityPresenter;
import pl.enteralt.demo.lsportfolio.presenter.splash.SplashActivityView;
import pl.enteralt.demo.lsportfolio.view.common.BaseActivity;
import pl.enteralt.demo.lsportfolio.view.main.activity.MainActivity;

public class SplashActivity extends BaseActivity implements SplashActivityView {

    @Inject
    SplashActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        presenter.startDelay();
    }

    /**
     * Initialize views
     */
    private void initViews() {
        ActivityComponentInitializer.init(this).inject(this);
        presenter.bindView(this);
    }

    @Override
    public void onSplashEnd() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }
}
