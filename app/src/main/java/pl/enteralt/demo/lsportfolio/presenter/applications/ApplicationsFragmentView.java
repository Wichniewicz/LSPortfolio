package pl.enteralt.demo.lsportfolio.presenter.applications;

import java.util.List;

import pl.enteralt.demo.lsportfolio.model.pojo.applications.Portfolio;
import pl.enteralt.demo.lsportfolio.view.applications.fragment.ApplicationsFragment;

/**
 * View for {@link ApplicationsFragment}
 */
public interface ApplicationsFragmentView {

    /**
     * Changed list of portfolios
     *
     * @param portfolios List of portfolios
     */
    void onPortfoliosGet(List<Portfolio> portfolios);

    /**
     * Received error of getting portolios
     */
    void onPortfoliosGetError();
}
