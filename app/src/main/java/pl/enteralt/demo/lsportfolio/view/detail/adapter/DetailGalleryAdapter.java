package pl.enteralt.demo.lsportfolio.view.detail.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.enteralt.demo.lsportfolio.R;
import pl.enteralt.demo.lsportfolio.view.detail.holder.DetailGalleryHolder;

/**
 * Adapter class for detail gallery item
 */
public class DetailGalleryAdapter extends RecyclerView.Adapter<DetailGalleryHolder> {

    /**
     * List of detail gallery items
     */
    private List<String> detailGalleryList = new ArrayList<>();

    /**
     * Listener for view holder
     */
    private DetailGalleryHolder.DetailGalleryHolderListener listener;

    public DetailGalleryAdapter(DetailGalleryHolder.DetailGalleryHolderListener listener) {
        this.listener = listener;
    }

    @Override
    public DetailGalleryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_gallery_holder, parent, false);
        return new DetailGalleryHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(DetailGalleryHolder holder, int position) {
        holder.setItem(detailGalleryList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return detailGalleryList.size();
    }

    /**
     * Add detail gallery items to list
     *
     * @param galleryItems List of detail gallery items
     */
    public void addDetailGalleryItems(List<String> galleryItems) {
        detailGalleryList.clear();
        detailGalleryList.addAll(galleryItems);
        notifyDataSetChanged();
    }
}
