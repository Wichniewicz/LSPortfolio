
package pl.enteralt.demo.lsportfolio.model.pojo.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Data {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("background")
    @Expose
    private String background;
    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("gallery")
    @Expose
    private List<String> gallery = new ArrayList<String>();
    @SerializedName("link")
    @Expose
    private List<Link> link = new ArrayList<Link>();

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * @return The background
     */
    public String getBackground() {
        return background;
    }

    /**
     * @param background The background
     */
    public void setBackground(String background) {
        this.background = background;
    }

    /**
     * @return The type
     */
    public Type getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * @return The gallery
     */
    public List<String> getGallery() {
        return gallery;
    }

    /**
     * @param gallery The gallery
     */
    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    /**
     * @return The link
     */
    public List<Link> getLink() {
        return link;
    }

    /**
     * @param link The link
     */
    public void setLink(List<Link> link) {
        this.link = link;
    }

}
