package pl.enteralt.demo.lsportfolio.presenter.contact;

import javax.inject.Inject;

import pl.enteralt.demo.lsportfolio.injector.scope.PerFragment;
import pl.enteralt.demo.lsportfolio.presenter.Presenter;
import pl.enteralt.demo.lsportfolio.view.contact.fragment.ContactFragment;

/**
 * Presenter for {@link ContactFragment}
 */
@PerFragment
public class ContactFragmentPresenter implements Presenter<ContactFragmentView> {

    /**
     * View controlled by presenter
     */
    private ContactFragmentView view;

    @Inject
    public ContactFragmentPresenter() {
    }

    /**
     * Bind view to control by presenter.
     *
     * @param view View to control by presenter.
     */
    @Override
    public void bindView(ContactFragmentView view) {
        this.view = view;
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onResume() method.
     */
    @Override
    public void resume() {

    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onPause() method.
     */
    @Override
    public void pause() {

    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onDestroy() method.
     */
    @Override
    public void destroy() {

    }

    /**
     * Open url
     *
     * @param url address to open
     */
    public void openUrl(String url) {
        view.onOpenUrl(url);
    }
}
