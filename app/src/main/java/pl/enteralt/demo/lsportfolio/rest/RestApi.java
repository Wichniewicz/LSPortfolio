package pl.enteralt.demo.lsportfolio.rest;

import pl.enteralt.demo.lsportfolio.model.pojo.applications.ApplicationItem;
import pl.enteralt.demo.lsportfolio.model.pojo.details.DetailItem;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * REST API service
 */
public interface RestApi {

    /**
     * Provides application list
     * @return {@link ApplicationItem}
     */
    @GET("/api/main")
    Call<ApplicationItem> getApplications();

    /**
     * Provides details about application
     * @param id application id
     * @return {@link DetailItem}
     */
    @GET("/api/product/{id}")
    Call<DetailItem> getApplicationDetails(@Path("id") int id);
}
