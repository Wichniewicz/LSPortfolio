package pl.enteralt.demo.lsportfolio.presenter.detail;

import javax.inject.Inject;

import pl.enteralt.demo.lsportfolio.injector.scope.PerActivity;
import pl.enteralt.demo.lsportfolio.model.pojo.details.DetailItem;
import pl.enteralt.demo.lsportfolio.presenter.Presenter;
import pl.enteralt.demo.lsportfolio.rest.RestApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Presenter for {@link pl.enteralt.demo.lsportfolio.view.detail.activity.DetailActivity}
 */
@PerActivity
public class DetailActivityPresenter implements Presenter<DetailActivityView>, Callback<DetailItem> {

    /**
     * View controlled by presenter
     */
    private DetailActivityView view;

    /**
     * REST api to make calls to webservice
     */
    private RestApi service;

    @Inject
    public DetailActivityPresenter(RestApi restApi) {
        this.service = restApi;
    }

    /**
     * Bind view to control by presenter.
     *
     * @param view View to control by presenter.
     */
    @Override
    public void bindView(DetailActivityView view) {
        this.view = view;
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onResume() method.
     */
    @Override
    public void resume() {
        // Empty
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onPause() method.
     */
    @Override
    public void pause() {
        // Empty
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onDestroy() method.
     */
    @Override
    public void destroy() {
        // Empty
    }

    /**
     * Retrieves application detail from REST API
     *
     * @param applicationId Application Id to get detail for
     */
    public void getApplicationDetailFromRest(int applicationId) {
        if (applicationId != 0) {
            Call<DetailItem> applicationDetail = service.getApplicationDetails(applicationId);
            applicationDetail.enqueue(this);
        } else {
            view.onApplicationDetailGetError();
        }
    }

    @Override
    public void onResponse(Call<DetailItem> call, Response<DetailItem> response) {
        try {
            view.onApplicationDetailGet(response.body().getData());
        } catch (Exception e) {
            view.onApplicationDetailGetError();
        }
    }

    @Override
    public void onFailure(Call<DetailItem> call, Throwable t) {
        view.onApplicationDetailGetError();
    }
}
