package pl.enteralt.demo.lsportfolio.view.contact.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.enteralt.demo.lsportfolio.R;
import pl.enteralt.demo.lsportfolio.injector.scope.PerFragment;
import pl.enteralt.demo.lsportfolio.presenter.contact.ContactFragmentPresenter;
import pl.enteralt.demo.lsportfolio.presenter.contact.ContactFragmentView;
import pl.enteralt.demo.lsportfolio.view.common.BaseFragment;

@PerFragment
public class ContactFragment extends BaseFragment implements ContactFragmentView {

    public static final String TAG = ContactFragment.class.getSimpleName();

    private static final String LOOKSOFT_URL = "http://public.looksoft.pl/portfolio/kontakt.html";

    @Bind(R.id.contactWebview)
    WebView webView;

    @Inject
    ContactFragmentPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        ButterKnife.bind(this, view);
        initDaggerComponents();
        initViews();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.openUrl(LOOKSOFT_URL);
    }

    /**
     * Initialize dagger component
     */
    @Override
    protected void initDaggerComponents() {
        FragmentComponentInitializer.init().inject(this);
    }

    /**
     * Initialize views
     */
    @Override
    protected void initViews() {
        presenter.bindView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * Open url
     *
     * @param url address to open
     */
    @Override
    public void onOpenUrl(final String url) {
        webView.loadUrl(url);
    }
}
