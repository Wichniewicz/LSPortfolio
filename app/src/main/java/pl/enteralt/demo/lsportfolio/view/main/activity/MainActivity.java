package pl.enteralt.demo.lsportfolio.view.main.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.enteralt.demo.lsportfolio.R;
import pl.enteralt.demo.lsportfolio.view.applications.fragment.ApplicationsFragment;
import pl.enteralt.demo.lsportfolio.view.common.BaseActivity;
import pl.enteralt.demo.lsportfolio.view.contact.fragment.ContactFragment;

public class MainActivity extends BaseActivity {

    @Bind(R.id.mainActivityToolbar)
    Toolbar toolbar;

    @Bind(R.id.commonToolbarTitle)
    TextView toolbarTitle;

    @Bind(R.id.mainDrawerLayout)
    DrawerLayout drawerLayout;

    @Bind(R.id.mainNavigationView)
    NavigationView navigationView;

    @Bind(R.id.mainContainer)
    FrameLayout mainContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpToolbar();
        setUpNavigationView();
        if (savedInstanceState == null) {
            replaceFragment(R.id.mainContainer, new ApplicationsFragment(), ApplicationsFragment.TAG).commit();
        }
    }

    /**
     * Sets navigation view for activity
     */
    private void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                navigationView.setCheckedItem(menuItem.getItemId());
                switch (menuItem.getItemId()) {
                    case R.id.menuMainDrawerApplications:
                        replaceFragment(R.id.mainContainer, new ApplicationsFragment(), ApplicationsFragment.TAG).commit();
                        break;
                    case R.id.menuMainDrawerContact:
                        replaceFragment(R.id.mainContainer, new ContactFragment(), ContactFragment.TAG).commit();
                        break;
                }
                toolbarTitle.setText(menuItem.getTitle());
                drawerLayout.closeDrawers();
                return true;
            }
        });
    }

    /**
     * Sets toolbar for activity
     */
    private void setUpToolbar() {
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.menu_icon);
            actionBar.setDisplayShowTitleEnabled(false);
            toolbarTitle.setText(R.string.menu_main_drawer_applications);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
