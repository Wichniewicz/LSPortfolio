package pl.enteralt.demo.lsportfolio.presenter.contact;

import pl.enteralt.demo.lsportfolio.view.contact.fragment.ContactFragment;

/**
 * View for {@link ContactFragment}
 */
public interface ContactFragmentView {

    /**
     * Open url
     *
     * @param url address to open
     */
    void onOpenUrl(String url);
}
