package pl.enteralt.demo.lsportfolio.presenter;

/**
 * Interface representing a Presenter in a model view presenter (MVP) pattern.
 *
 * @param <V> View to control by presenter.
 */
public interface Presenter<V> {

    /**
     * Bind view to control by presenter.
     *
     * @param view View to control by presenter.
     */
    void bindView(V view);

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onResume() method.
     */
    void resume();

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onPause() method.
     */
    void pause();

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onDestroy() method.
     */
    void destroy();
}
