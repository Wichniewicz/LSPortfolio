package pl.enteralt.demo.lsportfolio.presenter.detail;

import pl.enteralt.demo.lsportfolio.model.pojo.details.Data;

/**
 * View for {@link pl.enteralt.demo.lsportfolio.view.detail.activity.DetailActivity}
 */
public interface DetailActivityView {

    /**
     * Changed application detail
     *
     * @param applicationDetail Application detail
     */
    void onApplicationDetailGet(Data applicationDetail);

    /**
     * Received error on getting application detial
     */
    void onApplicationDetailGetError();
}
