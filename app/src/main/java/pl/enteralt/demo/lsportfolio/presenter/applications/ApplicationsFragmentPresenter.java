package pl.enteralt.demo.lsportfolio.presenter.applications;

import javax.inject.Inject;

import pl.enteralt.demo.lsportfolio.injector.scope.PerFragment;
import pl.enteralt.demo.lsportfolio.model.pojo.applications.ApplicationItem;
import pl.enteralt.demo.lsportfolio.presenter.Presenter;
import pl.enteralt.demo.lsportfolio.rest.RestApi;
import pl.enteralt.demo.lsportfolio.view.applications.fragment.ApplicationsFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Presenter for {@link ApplicationsFragment}
 */
@PerFragment
public class ApplicationsFragmentPresenter implements Presenter<ApplicationsFragmentView>, Callback<ApplicationItem> {

    /**
     * View controlled by presenter
     */
    private ApplicationsFragmentView view;

    /**
     * REST api to make calls to webservice
     */
    private RestApi service;

    @Inject
    public ApplicationsFragmentPresenter(RestApi restApi) {
        this.service = restApi;
    }

    /**
     * Bind view to control by presenter.
     *
     * @param view View to control by presenter.
     */
    @Override
    public void bindView(ApplicationsFragmentView view) {
        this.view = view;
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onResume() method.
     */
    @Override
    public void resume() {
        // Empty
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onPause() method.
     */
    @Override
    public void pause() {
        // Empty
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onDestroy() method.
     */
    @Override
    public void destroy() {
        // Empty
    }

    /**
     * Retrieves portfolios from REST API
     */
    public void getPortfoliosFromRest() {
        Call<ApplicationItem> applications = service.getApplications();
        applications.enqueue(this);
    }

    @Override
    public void onResponse(Call<ApplicationItem> call, Response<ApplicationItem> response) {
        try {
            view.onPortfoliosGet(response.body().getData().getPortfolio());
        } catch (Exception e) {
            e.printStackTrace();
            view.onPortfoliosGetError();
        }
    }

    @Override
    public void onFailure(Call<ApplicationItem> call, Throwable t) {
        view.onPortfoliosGetError();
    }
}
