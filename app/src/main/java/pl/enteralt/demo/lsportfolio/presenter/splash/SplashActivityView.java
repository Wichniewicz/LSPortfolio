package pl.enteralt.demo.lsportfolio.presenter.splash;

/**
 * View for {@link pl.enteralt.demo.lsportfolio.view.splash.activity.SplashActivity}
 */
public interface SplashActivityView {

    /**
     * End splash screen
     */
    void onSplashEnd();
}
